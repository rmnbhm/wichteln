﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Wichteln.Models;

namespace Wichteln.Models
{
    public class WichtelnContext : DbContext
    {
        public WichtelnContext (DbContextOptions<WichtelnContext> options)
            : base(options)
        {
        }

        public DbSet<Wichteln.Models.Participant> Participant { get; set; }

        public DbSet<Wichteln.Models.WichtelEvent> WichtelEvent { get; set; }
    }
}
