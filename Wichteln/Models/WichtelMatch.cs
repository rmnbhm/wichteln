﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wichteln.Models
{
    public class WichtelMatch
    {
        public Participant Giver { get; set; }
        public Participant Recipient { get; set; }
    }
}
