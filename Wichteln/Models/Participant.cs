﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Wichteln.Models
{
    public class Participant
    {
        public int ParticipantId { get; set; }

        [ForeignKey("WichtelEventId")]
        public virtual WichtelEvent WichtelEvent { get; set; }

        [Required]
        public string Name { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

    }
}
