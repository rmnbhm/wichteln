﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Wichteln.Models
{
    public class WichtelEvent
    {
        public int WichtelEventId { get; set; }

        [Display(Name = "Deadline")]
        [DataType(DataType.Date)]
        [NotBeforeToday]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime HeldAt { get; set; }

        [Display(Name = "Description")]
        [Required]
        public string EventName { get; set; }

        public IList<Participant> Participants { get; set; }

        [Display(Name = "Your Name")]
        [Required]
        public string InitiatorName { get; set; }

        [Display(Name = "Your Email")]
        [Required]
        [EmailAddress]
        public string InitiatorMail { get; set; }

        [Display(Name = "Max. Price of Gift in €")]
        [Required]
        [Range(0, int.MaxValue)]
        public int MaxValue { get; set; }
    }
}
