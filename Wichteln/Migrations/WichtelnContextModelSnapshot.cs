﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Wichteln.Models;

namespace Wichteln.Migrations
{
    [DbContext(typeof(WichtelnContext))]
    partial class WichtelnContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Wichteln.Models.Participant", b =>
                {
                    b.Property<int>("ParticipantId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int?>("WichtelEventId");

                    b.HasKey("ParticipantId");

                    b.HasIndex("WichtelEventId");

                    b.ToTable("Participant");
                });

            modelBuilder.Entity("Wichteln.Models.WichtelEvent", b =>
                {
                    b.Property<int>("WichtelEventId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("EventName")
                        .IsRequired();

                    b.Property<DateTime>("HeldAt");

                    b.Property<string>("InitiatorMail")
                        .IsRequired();

                    b.Property<string>("InitiatorName")
                        .IsRequired();

                    b.Property<int>("MaxValue");

                    b.HasKey("WichtelEventId");

                    b.ToTable("WichtelEvent");
                });

            modelBuilder.Entity("Wichteln.Models.Participant", b =>
                {
                    b.HasOne("Wichteln.Models.WichtelEvent", "WichtelEvent")
                        .WithMany("Participants")
                        .HasForeignKey("WichtelEventId");
                });
#pragma warning restore 612, 618
        }
    }
}
