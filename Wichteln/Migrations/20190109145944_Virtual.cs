﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wichteln.Migrations
{
    public partial class Virtual : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Participant_WichtelEvent_WichtelEventId",
                table: "Participant");

            migrationBuilder.AlterColumn<int>(
                name: "WichtelEventId",
                table: "Participant",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Participant_WichtelEvent_WichtelEventId",
                table: "Participant",
                column: "WichtelEventId",
                principalTable: "WichtelEvent",
                principalColumn: "WichtelEventId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Participant_WichtelEvent_WichtelEventId",
                table: "Participant");

            migrationBuilder.AlterColumn<int>(
                name: "WichtelEventId",
                table: "Participant",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Participant_WichtelEvent_WichtelEventId",
                table: "Participant",
                column: "WichtelEventId",
                principalTable: "WichtelEvent",
                principalColumn: "WichtelEventId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
