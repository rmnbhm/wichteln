﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wichteln.Models;

namespace Wichteln.Util
{
    public class Mailer
    {
        private string _SMTPServer;
        private int _Port;
        private string _User;
        private string _PW;

        public Mailer(string SMTPServer, int Port, string User, string PW)
        {
            _SMTPServer = SMTPServer;
            _Port = Port;
            _User = User;
            _PW = PW;
        }

        public Mailer(MailerConfig config)
        {
            _SMTPServer = config.SMTPServer;
            _Port = config.Port;
            _User = config.User;
            _PW = config.PW;
        }

        public void SendMail(WichtelEvent wichtelEvent, WichtelMatch match)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Wichtel Service", "wichteln@romanboehm.com"));
            message.To.Add(new MailboxAddress(match.Giver.Name, match.Giver.Email));
            message.Subject = $"{wichtelEvent.InitiatorName} invited you to to wichtel: {wichtelEvent.EventName}";

            message.Body = new TextPart("plain")
            {
                Text = $@"Dear {match.Giver.Name},

You have been invited to the following Wichtel Event: 

Description: {wichtelEvent.EventName}
Initiator: {wichtelEvent.InitiatorName}
Deadline: {wichtelEvent.HeldAt.ToString("MMMM dd, yyyy")}
Maximum Monetary Value of Your Gift in Euros: {wichtelEvent.MaxValue}
Recipient of Your Gift: {match.Recipient.Name}

If you have any questions, please contact {wichtelEvent.InitiatorName} at {wichtelEvent.InitiatorMail}.

(This Wichtel Event has been created for free using wichteln.romanboehm.com)"
            };

            using (var client = new SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect(_SMTPServer, _Port, true);

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(_User, _PW);

                client.Send(message);
                client.Disconnect(true);
            }

        }


    }
}
