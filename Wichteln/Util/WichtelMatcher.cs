﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wichteln.Models;

namespace Wichteln.Controllers
{
    public class WichtelMatcher
    {
        private static readonly Random rnd = new Random();
        public static IList<WichtelMatch> Match(IList<Participant> participants)
        {
            var givers = new List<Participant>(participants); // create defensive copy
            var recipients = ShiftParticipants(givers); // yes, shifting discloses all matches if one knows the order of entries
            IList<WichtelMatch> matches = new List<WichtelMatch>();
            for (int i = 0; i < participants.Count; i++)
            {
                matches.Add(new WichtelMatch
                {
                    Giver = givers[i],
                    Recipient = recipients[i]
                });
            }
            return matches;
        }

        private static IList<Participant> ShiftParticipants(List<Participant> participants)
        {
            int shiftBy = rnd.Next(1, participants.Count - 1); // bounds to make sure it does indeed get shuffled
            return ShiftParticipants(participants, shiftBy);
        }

        private static IList<Participant> ShiftParticipants(List<Participant> participants, int shiftBy)
        {
            var result = participants.GetRange(participants.Count - shiftBy, shiftBy);
            result.AddRange(participants.GetRange(0, participants.Count - shiftBy));
            return result;

        }
    }
}