﻿namespace Wichteln
{
    public class MailerConfig
    {
        public string SMTPServer { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string PW { get; set; }
    }
}