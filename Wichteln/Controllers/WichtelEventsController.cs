﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Wichteln.Models;
using Wichteln.Util;

namespace Wichteln.Controllers
{
    public class WichtelEventsController : Controller
    {
        private readonly WichtelnContext _context;
        private readonly IOptions<MailerConfig> _config;

        public WichtelEventsController(WichtelnContext context, IOptions<MailerConfig> config)
        {
            _config = config;
            _context = context;
        }

        // GET: WichtelEvents/Create
        public IActionResult Create()
        {
            var wichtelEvent = new WichtelEvent
            {
                Participants = new List<Participant> { new Participant(), new Participant() }, //ensure min. of two participants
                MaxValue = 0,
                HeldAt = DateTime.Now
            };
            return View(wichtelEvent);
        }

        // POST: WichtelEvents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("WichtelEventId,InitiatorName,InitiatorMail,HeldAt,EventName,MaxValue,Participants")] WichtelEvent wichtelEvent)
        {
            if (ModelState.IsValid)
            {
                foreach (var p in wichtelEvent.Participants)
                {
                    if (p.Name == null || p.Email == null || p.Name == "" || p.Email == "")
                    {
                        return View(wichtelEvent);
                    }
                }
                _context.Add(wichtelEvent);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { wichtelEventId = wichtelEvent.WichtelEventId });
            }
            return View(wichtelEvent);
        }

        // GET: WichtelEvents/Details/5
        public async Task<IActionResult> Details(int? wichtelEventId)
        {
            if (wichtelEventId == null)
            {
                return NotFound();
            }

            var wichtelEvent = await _context.WichtelEvent
                .Where(we => we.WichtelEventId == wichtelEventId.Value)
                .Include(we => we.Participants)
                .FirstOrDefaultAsync();
            if (wichtelEvent == null)
            {
                return NotFound();
            }

            return View(wichtelEvent);
        }

        // POST: WichtelEvents/Details/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Details(int wichtelEventId, [Bind("WichtelEventId,InitiatorName,InitiatorMail,HeldAt,EventName,MaxValue,Participants")] WichtelEvent wichtelEvent)
        {

            NotifyParticipants(wichtelEvent);
            DeleteEvent(wichtelEvent);
            return RedirectToAction(nameof(Create));
        }

        // GET: WichtelEvents/Edit/5
        public async Task<IActionResult> Edit(int? wichtelEventId)
        {
            if (wichtelEventId == null)
            {
                return NotFound();
            }

            var wichtelEvent = await _context.WichtelEvent
                .Where(we => we.WichtelEventId == wichtelEventId.Value)
                .Include(we => we.Participants)
                .FirstOrDefaultAsync();
            if (wichtelEvent == null)
            {
                return NotFound();
            }
            return View(wichtelEvent);
        }

        // POST: WichtelEvents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int wichtelEventId, [Bind("WichtelEventId,InitiatorName,InitiatorMail,HeldAt,EventName,MaxValue,Participants")] WichtelEvent wichtelEvent)
        {
            if (wichtelEventId != wichtelEvent.WichtelEventId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(wichtelEvent);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Details), new { wichtelEventId = wichtelEvent.WichtelEventId });
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WichtelEventExists(wichtelEvent.WichtelEventId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View(wichtelEvent);
        }

        // GET: WichtelEvents/Delete/5
        public async Task<IActionResult> Delete(int? wichtelEventId)
        {
            if (wichtelEventId == null)
            {
                return NotFound();
            }

            var wichtelEvent = await _context.WichtelEvent
                .Where(we => we.WichtelEventId == wichtelEventId.Value)
                .Include(we => we.Participants)
                .FirstOrDefaultAsync();
            if (wichtelEvent == null)
            {
                return NotFound();
            }

            return View(wichtelEvent);
        }

        // POST: WichtelEvents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int wichtelEventId)
        {
            var wichtelEvent = await _context.WichtelEvent.Where(we => we.WichtelEventId == wichtelEventId)
                .Include(we => we.Participants)
                .FirstOrDefaultAsync();
            _context.WichtelEvent.Remove(wichtelEvent);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Create));
        }

        private bool WichtelEventExists(int wichtelEventId)
        {
            return _context.WichtelEvent.Any(e => e.WichtelEventId == wichtelEventId);
        }

        private void DeleteEvent(WichtelEvent wichtelEvent)
        {
            _context.WichtelEvent.Remove(wichtelEvent);
            _context.SaveChangesAsync();
        }

        private void NotifyParticipants(WichtelEvent wichtelEvent)
        {
            var matches = WichtelMatcher.Match(wichtelEvent.Participants);

            //TODO: read settings from config

            var mailer = new Mailer(_config.Value);
            foreach (var match in matches)
            {
                mailer.SendMail(wichtelEvent, match);
            }
        }
    }
}
