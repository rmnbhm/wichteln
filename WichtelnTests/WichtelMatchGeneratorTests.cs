using System;
using System.Collections.Generic;
using System.Linq;
using Wichteln.Controllers;
using Wichteln.Models;
using Xunit;

namespace WichtelnTests
{
    public class WichtelMatchGeneratorTests
    {
        [Fact]
        public void TestMatch_EachAsGiverAndRecipient()
        {
            var participants = new List<Participant>
            {
                new Participant { Name = "p1" },
                new Participant { Name = "p2" },
                new Participant { Name = "p3" }
            };
            var matches = WichtelMatcher.Match(participants);
            var givers = matches.Select(m => m.Giver);
            var recipients = matches.Select(m => m.Recipient);
            Assert.True(participants.All(p => givers.Contains(p)));
            Assert.True(participants.All(p => recipients.Contains(p)));
        }

        [Fact]
        public void TestMatch_Shift()
        {
            var participants = new List<Participant>
            {
                new Participant { Name = "p1" },
                new Participant { Name = "p2" },
                new Participant { Name = "p3" }
            };
            var matches = WichtelMatcher.Match(participants);
            foreach (var match in matches)
            {
                Assert.NotStrictEqual<Participant>(match.Giver, match.Recipient);
            }
        }
    }
}
